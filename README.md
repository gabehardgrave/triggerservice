# Trigger Service

This application samples the runner buddy database, and notifies the filtering service of new, updated data.

## Setup and Testing

Significant portions of this application use the Spring framework, however, gradle should handle all of the dependencies for you.

Before you start testing, make sure assertions are enabled. (This should be set in the build.gradle file already, although manual or eclipse runs might not do this.)

To test the application, simple run


```
#!shell

./gradlew bootRun
```

from the root directory.

You should see the generic, Spring application boilerplate, before task related information is printed to the command line.

There are two asynchronous tasks that Trigger Service currently runs; fetching update data, and forwarding updates to the filtering service.

You will know the application is fetching update data because you will see the following:

```
Jun 21, 2017 10:35:32 AM trigger.TriggerManager checkForUpdates
INFO: Checking Database for Updates
Jun 21, 2017 10:35:32 AM trigger.TriggerManager checkForUpdates
INFO: 11 Updates Fetched
```

The exact number is unimportant, although it should correspond to the number of distinct records in the user_updates table (distinct over <user_id, date> pairs that is, not necessarily the number of records in the table). Right now, this info is output through STDERR, although that is because of how Java's logging utilities work, not because an error occurred.

You will know the application is sending update data because you will see the following:

```shell
Sending Update Data . . .
Sending :: Update[ /* fields */ ]
Finished Sending Update Data
```

Note that you will see a `Sending :: Update[ /* fields */ ]` for each update entry fetched in the previous call to check for updates. The number should be exactly equal to the number of updates fetched in the most recent call to `checkForUpdates()`, which could be 0. Update data that is successfully sent is immediately discarded to make room for future updates.


## Notes on the current build (important for testing)

Right now, the filtering service is NOT set up to receive any data, so update data is not actually sent anywhere. It is just printed to STDOUT for debugging purposes. This means that right now, all update data will trivially be "sent successfully", so successive calls to `notifyFilterService()` (the method that sends the update data) shouldn't be sending any update data, until new data comes up after `checkForUpdates()` is called.

In addition, the current build requires a connection the RunnerBuddyTest database to function. The information for connecting to the RunnerBuddyTest database is in `/config/connection.properties`. If a connection cannot be made, the TriggerService will attempt to connect to an in-memory database, which is currently unimplemented (and it's not clear that implementation is necessary, or even useful).

TriggerService currently does not modify the RunnerBuddyTest database in any way, which is necessary for testing, although that needs to be changed in production builds, to avoid pulling duplicate data.

Finally, `notifyFilterService()` and `checkForUpdates()` are currently scheduled at fixed intervals (this can be seen by the `@Scheduled(fixedRate=. . .)` annotations, in TriggerManager.java). At the moment, these values are **totally arbitrary**. You can and should modify these for testing, and the output should still be consistent. For future production builds, these should be configured to more appropriate values.