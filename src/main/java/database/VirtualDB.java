/**
 * 
 */
package database;

import java.util.Set;

import trigger.UpdateData;

/**
 * @author gshardgrave
 *
 */
public class VirtualDB implements DatabaseAccessor {

    private static VirtualDB db_interface = new VirtualDB();
    
    /**
     * Enforce single connection.
     * @return Singleton instance of a RealDB Object
     */
    public static VirtualDB getDB() {
        return db_interface;
    }
    
    private VirtualDB() {
        throw new UnsupportedOperationException("UNIMPLEMENTED");
    }

    @Override
    public void fetchUserUpdates(Set<UpdateData> to_fill) /*throws SQLException*/ {
        throw new UnsupportedOperationException("UNIMPLEMENTED");
    }
}
