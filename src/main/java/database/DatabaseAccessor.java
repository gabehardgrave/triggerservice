/**
 * 
 */
package database;

import java.sql.SQLException;
import java.util.Set;

import trigger.UpdateData;

/**
 * For pulling and writing data to "the database" (virtual or real)
 * @author gshardgrave
 */
public interface DatabaseAccessor {

    /**
     * Adds new update data to to_fill, from the runnerbuddy database. Elements in to_fill will not
     * be removed.
     * 
     * @param to_fill
     * @throws SQLException
     */
    public void fetchUserUpdates(Set<UpdateData> to_fill) throws SQLException;
    
}