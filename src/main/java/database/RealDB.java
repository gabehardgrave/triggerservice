package database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Set;

import trigger.UpdateData;

/**
 * @author gshardgrave
 */
public class RealDB implements DatabaseAccessor { // TODO: implement caching

    private Properties connection_props = null;
    private Connection connection = null;
    private PreparedStatement select_stmt = null;
    private static RealDB db_interface = null;
    
    /**
     * Enforce single connection.
     * @return Singleton instance of a RealDB Object
     * @throws SQLException     Lets caller know to use VirtualDB Instead
     */
    public static RealDB getDB() throws SQLException {
        
        if (RealDB.db_interface == null) {
            RealDB.db_interface = new RealDB();
        }
        
        return db_interface;
    }
    
    
    /**
     * Initializes the connection to the Runnerbuddy test database, and prepares the necessary
     * statements for fetching and updating resources.
     * @throws SQLException 
     */
    private RealDB() throws SQLException {
        
        this.connection_props = new Properties();
        String url = null;
        String username = null;
        String password = null;
        
        try (FileInputStream prop_file = new FileInputStream("config/connection.properties")) {
            
            this.connection_props.load(prop_file);
            url = this.connection_props.getProperty("dburl");
            username = this.connection_props.getProperty("username");
            password = this.connection_props.getProperty("password");
            
            /*System.out.println("dburl = " + url);
            System.out.println("username = " + username);
            System.out.println("password = " + password);*/

            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(url, username, password);
            this.select_stmt = this.connection.prepareStatement(UpdateData.select_query());
            
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("NO DRIVER", e);
        }
        
        /*
        String url = "jdbc:mysql://208.109.53.180:3306/runnerbuddytest";
        String username = "runnerbuddy";
        String password = "Chang3me!";*/


    }


    @Override
    public void fetchUserUpdates(Set<UpdateData> to_fill) throws SQLException {
        
        try (ResultSet results = this.select_stmt.executeQuery())
        {
            while (results.next()) {
                UpdateData entity = new UpdateData(results);
                to_fill.add(entity);
            }
            
        }
    }
    

    
}
