/**
 * 
 */
package trigger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author gshardgrave
 *
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class Application {

    
    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication trigger_app = new SpringApplication(Application.class);
        trigger_app.run(args);
    }
}
