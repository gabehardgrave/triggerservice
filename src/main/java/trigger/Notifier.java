/**
 * 
 */
package trigger;

import java.util.ArrayList;
import java.util.Collection;

import trigger.UpdateData;

/**
 * Sends messages to the Filtering System
 * Since Filtering System is not yet configured to accept data, this just prints to STDOUT
 * 
 * @author gshardgrave
 */
public class Notifier {

    
    /**
     * Attempts to send all elements in to_send, returning a collection of all sent entries in
     * to_send.
     * 
     * Right now, just prints to STDOUT, since the Filtering System isn't configured to accept
     * data. 
     * 
     * @param to_send
     * @return Collection<UpdateData>
     */
    public static Collection<UpdateData> send(Collection<UpdateData> to_send) {
        Collection<UpdateData> sent = new ArrayList<>(to_send.size());
        
        System.out.println("Sending Update Data . . .");
        for (UpdateData data: to_send) {
            System.out.println("Sending :: " + data.toString());
            sent.add(data); // in practice, would only happen if data was sent successfully
        }
        System.out.println("Finished Sending Update Data");
        
        return sent;
    }
    
}
