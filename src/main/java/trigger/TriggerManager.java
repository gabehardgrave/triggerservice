/**
 * 
 */
package trigger;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import database.DatabaseAccessor;
import database.RealDB;
import database.VirtualDB;

/**
 * Trigger
 * 
 * Regularly samples the Runnerbuddy data base for updated users, and forwards information on who
 * should be updated to Filtering Service.
 * 
 * @author gshardgrave
 */
@Component
public class TriggerManager {
    
    /* Interface for fetching resources from "the database" */
    private DatabaseAccessor db = null;
    
    private final static Logger LOGGER = Logger.getLogger(TriggerManager.class.getName());
    
    // Constants used in constructing storage for updated data. Since update_data is a HashMap,
    // this should ideally be equal to (maximum number of expected updates) / (hash map load factor)
    private static final float LOAD_FACT = 0.66f;
    private static final int EXPECTED_MAX_NUM = 80;
    private static final int DEF_SIZE = (int) (EXPECTED_MAX_NUM / LOAD_FACT);
    
    /* Storage for temporary users. ABSOLUTELY MUST BE THREADSAFE */
    private final Set<UpdateData> updated_data = Collections.newSetFromMap(
                                                   new ConcurrentHashMap<UpdateData, Boolean>(
                                                                               DEF_SIZE, LOAD_FACT
                                                                          ));
    /**
     */
    public TriggerManager() {
        this.resetDB();
        assert(this.db != null);
        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(new ConsoleHandler());
        
    }
    
    /**
     * Tells the Filtering Service to pull observation data, and dumps updated data
     */
    @Async
    @Scheduled(fixedRate=1000)
    public void notifyFilterService() {
        assert(this.updated_data != null);
        Collection<UpdateData> to_dump = Notifier.send(this.updated_data);
        this.updated_data.removeAll(to_dump);
    }
    
    
    /**
     * Samples the Database for updated users. 
     */
    @Async
    @Scheduled(fixedRate=5000)
    public void checkForUpdates() {
        assert(this.db != null);
        LOGGER.info("Checking Database for Updates");
        try {
            this.db.fetchUserUpdates(this.updated_data);
            MessageFormat fmt = new MessageFormat("{0} Updates Fetched");
            LOGGER.info(fmt.format( new Object[] {new Integer(this.updated_data.size()) } ));
        } catch (SQLException e) {
            LOGGER.info(e.toString());
            this.resetDB();
            assert(this.db != null);
        }
    }
    
    
    // Attempts to set the current database accessor to the Real Database. Sets it to a virtual,
    // in memory database if the connection fails.
    private void resetDB() {
        try {
            this.db = RealDB.getDB();
        } catch (SQLException e) {
            LOGGER.info("Error connecting to Runnerbuddy Database.");
            LOGGER.info(e.toString());
            this.db = VirtualDB.getDB();
        }
    }
}
