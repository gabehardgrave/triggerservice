/**
 * 
 */
package trigger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * @author gshardgrave
 *
 */
public class Utils {

    /**
     * zips the two collections together. 
     */
    public static <T> List<T[]> zip(List<T> list1, List<T> list2) {
        List<T[]> zipped = new ArrayList<T[]>();
        
        Iterator<T> iter_1 = list1.iterator();
        Iterator<T> iter_2 = list2.iterator();
        
        while (iter_1.hasNext() && iter_2.hasNext()) {
            T[] tuple = (T[]) new Object[]{iter_1.next(), iter_2.next()};
            zipped.add(tuple);
        }
        
        return zipped;
    }
    
}
