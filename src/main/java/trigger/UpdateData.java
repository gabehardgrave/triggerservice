/**
 * 
 */
package trigger;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Immutable representation of necessary Update Data
 * 
 * @author gshardgrave
 */
public final class UpdateData {
    
    private final long user_id;
    private final String name;
    private final String date;
    
    /**
     * @param user_id
     * @param name
     * @param date 
     */
    public UpdateData(long user_id, String name, String date) {
        this.user_id = user_id;
        this.name = name;
        this.date = date;
    }
    
    /**
     * Creates UpdateData from SQL query. Should be consistent with UpdateData.query()
     * 
     * @param result
     * @throws SQLException
     */
    public UpdateData(ResultSet result) throws SQLException {
        this.user_id = result.getLong("user_id");
        this.name = result.getString("name");
        this.date = result.getString("date");
    }

    /**
     * Based on the current state of the database. Should be consistent with Constructer and
     * internal data.
     * @return SQL query for fetching update data.
     */
    public static String select_query() {
        return ("SELECT DISTINCT user_id, name, date " +
                "FROM runnerbuddytest.users JOIN runnerbuddytest.user_updates ON " + 
                "runnerbuddytest.users.user_id = runnerbuddytest.user_updates.uid " + 
                "WHERE runnerbuddytest.user_updates.flag=0");
    }
    
    /**
     * 
     */
    @Override
    public String toString() {
        assert(this.name != null && this.date != null);
        String repr = String.format("Update[user_id=%d, name='%s', date='%s']",
                                    new Long(this.user_id), this.name, this.date);
        return repr;
    }
    
    
    @Override
    public int hashCode() {
        // Only use this.user_id and this.user_date, since this.user_id and this.name should
        // already be uniquely associated (if the database is configured properly)
        assert(this.name != null && this.date != null);
        final int prime = 37;
        long l = this.user_id;
        
        int result = prime + ((int) (l ^ (l >>> 32)));
        result = (prime * result) +  this.date.hashCode();
        return result;
    }
    
    @Override
    public boolean equals(Object o) {
        assert(this.name != null && this.date != null);
    
        if (o instanceof UpdateData) {
            UpdateData other = (UpdateData) o;
            assert(other.name != null && other.date != null);
            
            // If either check fails, it means something is wrong with the database
            assert(other.name.equals(this.name) || other.user_id != this.user_id);
            assert(other.user_id != this.user_id || other.name.equals(this.name));
            
            // exploit the fact that x.user_id == y.user_id implies x.username == y.username
            // (assuming the table is configured properly)
            return (other.user_id == this.user_id && other.date.equals(this.date));
        }
        return false;
    }
    
    /**
     * @return long
     */
    public long getUser_id() {
        assert(this.name != null && this.date != null);
        return this.user_id;
    }

    /**
     * @return String
     */
    public String getName() {
        assert(this.name != null && this.date != null);
        return this.name;
    }
    
    /**
     * @return String
     */
    public String getDate() {
        assert(this.name != null && this.date != null);
        return this.date;
    }
}
